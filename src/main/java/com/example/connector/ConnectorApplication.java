package com.example.connector;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import java.io.File;

@SpringBootApplication
public class ConnectorApplication {

	public static void main(String[] args) {
		String fileName = "snmplog.txt";
		System.setProperty("app.filepath", System.getProperty("user.dir") + File.separator + fileName);
		SpringApplication.run(ConnectorApplication.class, args);
	}
}
