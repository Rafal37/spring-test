package com.example.connector.Controllers;

import com.example.connector.Services.FileService;
import org.apache.catalina.webresources.Cache;
import org.apache.catalina.webresources.FileResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.File;
import java.lang.reflect.MalformedParameterizedTypeException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@RestController
public class FileController {

    private FileService fileService;

    @Autowired
    public FileController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping(value = "/file.txt", produces = "application/force-download")
    public FileSystemResource downloadFile() {

        String pathStr = System.getProperty("app.filepath");

        if (pathStr == null) {
            return null;
        }

        Path path = Paths.get(pathStr);

        if (Files.exists(path)) {
            return new FileSystemResource(pathStr);
        } else {
            return null;
        }
    }
}
