package com.example.connector.Controllers;

import com.example.connector.Services.FileService;
import com.example.connector.Services.SNMPService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
public class SNMPController {

    private final String COMMUNITY_STRING = "public";
    private final String HOME_ADRESS = "127.0.0.1";
    private final String CRACKOW_ADRESS = "149.156.163.104";
    private final String GDANSK_ADRESS = "153.19.121.167";

    private final String REQUESTED_ITEM = "1.3.6.1.2.1.1.1.0";

    private SNMPService SNMPService;
    private FileService fileService;

    @Autowired
    public SNMPController(SNMPService snmpService, FileService fileService) {
        this.SNMPService = snmpService;
        this.fileService = fileService;
    }

    @GetMapping("/home")
    public String connectToHome() {
        return connect(HOME_ADRESS);
    }

    @GetMapping("/crackow")
    public String connectToCrackow() {
        return connect(CRACKOW_ADRESS);
    }

    @GetMapping("/gdansk")
    public String connectToGdansk() {
        return connect(GDANSK_ADRESS);
    }

    private String connect(String ipAdress) {
        boolean connected = SNMPService.Run();
        String message = "";

        try {
            message = connected ? SNMPService.getItem(ipAdress, COMMUNITY_STRING, REQUESTED_ITEM) : "Error: Could not connect to " + ipAdress;
        } catch (Exception e) {
            return "Error: not connected";
        }
        
        fileService.saveToFile(System.getProperty("app.filepath"), message);
        return message;
    }
}
