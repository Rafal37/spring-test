package com.example.connector.Services;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

@Service
public class FileService {

    public boolean saveToFile(String absolutePath, String content) {

        if (StringUtils.isEmpty(absolutePath) || content == null) {
            return false;
        }

        File f = new File(absolutePath);
        Path path = Paths.get(absolutePath);
        f.getParentFile().mkdirs();

        try {
            f.createNewFile();
            Files.write(path, (content + System.getProperty("line.separator")).getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            return false;
        }

        return Files.exists(path);
    }
}
