package com.example.connector.Services;

import org.apache.logging.log4j.util.Strings;
import org.snmp4j.CommunityTarget;
import org.snmp4j.PDU;
import org.snmp4j.Snmp;
import org.snmp4j.Target;
import org.snmp4j.event.ResponseEvent;
import org.snmp4j.mp.SnmpConstants;
import org.snmp4j.smi.OID;
import org.snmp4j.smi.OctetString;
import org.snmp4j.smi.UdpAddress;
import org.snmp4j.smi.VariableBinding;
import org.snmp4j.transport.DefaultUdpTransportMapping;
import org.springframework.stereotype.Service;


@Service
public class SNMPService {

    //host: grodzpc-163-104.phils.uj.edu.pl
    //ip: 149.156.163.104

    //host: p-117-6.zr.univ.gda.pl
    //ip: 153.19.121.167

    //port: 161
    //OID: (.1.3.6.1.2.1.1.1.0)

    //snmpget -H
    //snmpget -v 2c -c public 127.0.0.1 .1.3.6.1.2.1.1.1.0

    private final int PORT = 161;
    private final int MAX_RETRIES = 3;
    private final int TIMEOUT = 2000;

    private Snmp snmp;

    public boolean Run() {
        try {
            snmp = new Snmp(new DefaultUdpTransportMapping());
            snmp.listen();
        } catch (Exception e) {
            System.out.print("Smnp setup error");
            return false;
        }
        return true;
    }

    private String buildAdress(String ip) {
        return ip + "/" + PORT;
    }

    public String getItem(String ip, String communityString, String oidStr) {

        if (snmp == null) {
            return null;
        }

        if (Strings.isEmpty(ip) || Strings.isEmpty(communityString) || Strings.isEmpty(oidStr)) {
            throw new IllegalArgumentException("Ip: " + ip + " Community String: " + communityString + " OID: " + oidStr);
        }

        String adress = buildAdress(ip);
        OID oid = new OID(oidStr);

        ResponseEvent event = createResponseEvent(oid, communityString, adress);

        return (event.getResponse() == null) ?
                null
                    :
                event.getResponse().get(0).getVariable().toString();
    }

    private ResponseEvent createResponseEvent(OID oid, String communityString, String adress) {
        PDU pdu = new PDU();
        pdu.add(new VariableBinding(oid));
        pdu.setType(PDU.GET);

        try {
            return snmp.get(pdu, getTarget(communityString, adress));
        } catch (Exception e) {
            return null;
        }
    }

    private Target getTarget(String communityString, String adress) throws IllegalArgumentException {
        CommunityTarget target = new CommunityTarget();
        target.setCommunity(new OctetString(communityString));
        target.setAddress(new UdpAddress(adress));
        target.setRetries(MAX_RETRIES);
        target.setTimeout(TIMEOUT);
        target.setVersion(SnmpConstants.version2c);
        return target;
    }
}
