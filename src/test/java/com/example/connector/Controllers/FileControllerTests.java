package com.example.connector.Controllers;

import com.example.connector.Services.FileService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Paths;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = FileController.class)
public class FileControllerTests {

    private final String TEST_PATH = "/testdir/test.txt";
    private final String TEST_CONTENT = "Test content";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileService;

    @BeforeEach
    void init() {
        given(this.fileService.saveToFile(TEST_PATH, TEST_CONTENT)).willReturn(true);
    }

    @Test
    void testDownloadingFile() throws Exception{
        mockMvc.perform(get("/file.txt")).andExpect(status().isOk());
    }
}
