package com.example.connector.Controllers;

import com.example.connector.Services.FileService;
import com.example.connector.Services.SNMPService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = SNMPController.class)
public class SNMPControllerTests {

    private final String TEST_PATH = "/testdir/test.txt";
    private final String TEST_CONTENT = "Test content";

    private final String IP_ADRESS = "127.0.0.1";
    private final String COMMUNITY_STRING = "public";
    private final String REQUESTED_OID = "1.3.6.1.2.1.1.1.0";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private FileService fileService;

    @MockBean
    private SNMPService snmpService;

    @BeforeEach
    void init() {
        given(this.fileService.saveToFile(TEST_PATH, TEST_CONTENT)).willReturn(true);
        given(this.snmpService.getItem(IP_ADRESS, COMMUNITY_STRING, REQUESTED_OID)).willReturn("Requested Item");
    }

    @Test
    void testHome() throws Exception {
        mockMvc.perform(get("/home")).andExpect(status().isOk());
    }

    @Test
    void testCrackow() throws Exception {
        mockMvc.perform(get("/crackow")).andExpect(status().isOk());
    }

    @Test
    void testGdansk() throws Exception {
        mockMvc.perform(get("/gdansk")).andExpect(status().isOk());
    }
}
