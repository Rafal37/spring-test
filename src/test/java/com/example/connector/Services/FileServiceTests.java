package com.example.connector.Services;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileServiceTests {

    private FileService fileService;

    private final String TEST_PATH = "/testdir/test.txt";
    private final String TEST_CONTENT = "Test content";

    public FileServiceTests() {
        fileService = new FileService();
    }

    @BeforeEach
    void clearFiles() throws IOException {
        Files.deleteIfExists(Paths.get(TEST_PATH));
    }

    @Test
    void saveToFile_shouldNotCreateFileIfNullParams() {
        String path = null;
        String content = null;

        boolean result = fileService.saveToFile(path, content);

        Assert.isTrue(!result, "File must not be created when null params are given");
    }

    @Test
    void saveToFile_shouldNotCreateFileIfNullPath() {
        String path = null;

        boolean result = fileService.saveToFile(path, TEST_CONTENT);

        Assert.isTrue(!result, "Function should return false but no throw exception");
    }

    @Test
    void saveToFile_shouldNotCreateFileIfNullContent() {
        String content = null;

        boolean result = fileService.saveToFile(TEST_PATH, content);

        Assert.isTrue(!result, "Function should return false but no throw exception");
    }

    @Test
    void saveToFile_shouldNotCreateFileIfEmptyPath() {
        String pathStr = "";

        boolean result = fileService.saveToFile(pathStr, TEST_CONTENT);

        Assert.isTrue(!result, "Function should return false but no throw exception");
    }

    @Test
    void saveToFile_shouldCreateFileIfEmptyContent() throws IOException {
        String content = "";
        Path path = Paths.get(TEST_PATH);

        boolean result = fileService.saveToFile(TEST_PATH, content);

        Assert.isTrue(result, "Function should finish properly");
        Assert.isTrue(Files.exists(path), "File should exist");
    }

    @Test
    void saveToFile_doesNotOverrideFileContent() {
        long singleContentLenght = TEST_CONTENT.length();
        long newLineSymbolLenght = System.getProperty("line.separator").getBytes().length;
        long correctLength = singleContentLenght*2 + newLineSymbolLenght*2;

        boolean result1 = fileService.saveToFile(TEST_PATH, TEST_CONTENT);
        boolean result2 = fileService.saveToFile(TEST_PATH, TEST_CONTENT);
        File file = new File(TEST_PATH);

        Assert.isTrue(result1 && result2, "Both write-to-file attempts should be successfull");
        Assert.isTrue(Files.exists(Paths.get(TEST_PATH)), "File should exist");
        Assert.isTrue(file.length() == correctLength , "File has twice as bits as single test-content");
    }
}
