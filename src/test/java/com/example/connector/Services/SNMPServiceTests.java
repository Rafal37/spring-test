package com.example.connector.Services;

import org.assertj.core.util.Strings;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;

import java.io.IOException;

public class SNMPServiceTests {

    private SNMPService snmpService;

    private final String IP_ADRESS = "127.0.0.1";
    private final String COMMUNITY_STRING = "public";
    private final String REQUESTED_OID = "1.3.6.1.2.1.1.1.0";

    @BeforeEach
    void initializeService() {
        snmpService = new SNMPService();
    }

    @Test
    void getItem_shouldThrowExceptionOnNullIp() {
        boolean success = false;
        snmpService.Run();

        try {
            snmpService.getItem(null, COMMUNITY_STRING, REQUESTED_OID);
        } catch (Exception e) {
            success = true;
        }

        Assert.isTrue(success, "Exception is thrown on null ip");
    }

    @Test
    void getItem_shouldThrowExceptionOnNullCommunityString() {
        boolean success = false;
        snmpService.Run();

        try {
            snmpService.getItem(IP_ADRESS, null, REQUESTED_OID);
        } catch (Exception e) {
            success = true;
        }

        Assert.isTrue(success, "Exception is thrown on null ip");
    }

    @Test
    void getItem_shouldThrowExceptionOnNullRequestedObject() {
        boolean success = false;
        snmpService.Run();

        try {
            snmpService.getItem(IP_ADRESS, COMMUNITY_STRING, null);
        } catch (Exception e) {
            success = true;
        }

        Assert.isTrue(success, "Exception is thrown on null ip");
    }

    @Test
    void getItem_shouldReturnNullIfNotRunning() {
        String response;

        response = snmpService.getItem(IP_ADRESS, COMMUNITY_STRING, REQUESTED_OID);

        Assert.isNull(response, "Null response if service is not running");
    }

    @Test
    void getItem_shouldPassIfRunningAndCorrectArgs() {
        String objectInfo = "";
        snmpService.Run();

        objectInfo = snmpService.getItem(IP_ADRESS, COMMUNITY_STRING, REQUESTED_OID);

        Assert.isTrue(!Strings.isNullOrEmpty(objectInfo), "Returned string is not empty or null");
    }
}
